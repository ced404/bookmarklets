
/*
	Open links in Office Apps (for mobile)

	activate and click any link to open URL in Office Online

	javascript:for(var links=document.querySelectorAll("a"),l=links.length,viewInOfficeOnline=function(e){return e.preventDefault(),window.open(e.target.dataset.officeUrl)};l--;){var officeURL="https://view.officeapps.live.com/op/view.aspx?src="+links[l].getAttribute("href");links[l].dataset.officeUrl=officeURL,links[l].addEventListener("click",viewInOfficeOnline)}

*/

var links = document.querySelectorAll ('a'),
		l = links.length;

var viewInOfficeOnline = function (event) {
	event.preventDefault();
	return window.open ( event.target.dataset.officeUrl );
};

while (l--) {
	var officeURL = 'https://view.officeapps.live.com/op/view.aspx?src=' + links[l].getAttribute('href');
	links[l].dataset.officeUrl = officeURL;
	links[l].addEventListener ('click', viewInOfficeOnline);
}
