# Bookmarklets

## Bookmarklets on mobile (Google Chrome)

- Add bookmarklets on Chrome desktop (give it a nice name ex. "killPopups!", eventually with icons http://graphemica.com/search?q=bomb)
- Sync your bookmarks
- On Chrome mobile, start typing the bookmarklet title, it will autocomplete




## Replace Images

Replace all `<img>` with a placeholder from http://placehold.it/ showing the image dimensions : [replace-images.js](https://bitbucket.org/ced404/bookmarklets/src/b4fdec8ad17d365a94779ea05777e08979a830ca/replace-images.js?at=master&fileviewer=file-view-default)

![placehold.it](http://placehold.it/350x150/ff00ff/ffffff)

## Convert Case

Convert selected text to lowercase, uppercase or title case.

## Edit Toggle

Make the document body "content editable" (toggle)

## Instagram Filters

Right-click images to apply some filters

## Kill Popups

Remove div's with high z-index (>666)

May kill menus, overlays, etc.

(*Very* useful on mobile ;)

p.s. : https://www.foaas.com/off/Popups/ced404

## QR Encode current URL

useful to open desktop URL on mobile

https://play.google.com/store/apps/details?id=com.google.zxing.client.android

## View in Office Online

click + (metaKey || ctrlKey || shiftKey) on any link to open the URL in Office Online.

https://products.office.com/en-us/office-online/view-office-documents-online

(Google search results for instance ;) https://www.google.fr?q=login+password+filetype:xls)

## Notes

Alchemize : https://alchemizeapp.com/
