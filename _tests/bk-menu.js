
/* bookmarlets funk */

var officeapps = function () {

	var links = document.querySelectorAll ('a'),
	l = links.length;

	var viewInOfficeOnline = function (event) {
		if ( event.metaKey || event.ctrlKey || event.shiftKey ) {
			event.preventDefault();
			return window.open ( event.target.dataset.officeUrl );
		}
	};

	while (l--) {
		var officeURL = 'https://view.officeapps.live.com/op/view.aspx?src=' + links[l].getAttribute('href');
		links[l].dataset.officeUrl = officeURL;
		links[l].addEventListener ('click', viewInOfficeOnline);
	}

}; // officeapps



// bookmarlets menu

var items = [
	{
		"caption" : "View in Office Online",
		"funk" : officeapps,
	}
];

var menu = document.createElement ('details'),
		summary = document.createElement ('summary'),
		body = document.body;

menu.classList.add ('bk-menu');
summary.classList.add ('bk-menu-summary');
summary.innerText = 'Bookmarklet menu';
menu.appendChild (summary);

var l = items.length;

while (l--) {
	var item = items[l];
	var menuItem = document.createElement ('a');
	var href = "javascript:" + ('' + item.funk).replace( new RegExp("\n", 'g'), "");
	menuItem.setAttribute ("href", href);
	menuItem.innerText = item.caption;
	menu.appendChild (menuItem);
}


body.appendChild (menu);
