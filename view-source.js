(function (document) {
	
	// https://css-tricks.com/snippets/javascript/htmlentities-for-javascript/
	const htmlEntities = function (str) {
		return str.replace(/\n/g, "\n").replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;').replace(/"/g, '&quot;');
	};
	
	let sourceTitle = document.title;
	// let source = document.documentElement.outerHTML;
	let source = document.documentElement.innerHTML;
	source = htmlEntities (`<html>${source}</html>`);
	let prismCDN = 'https://cdnjs.cloudflare.com/ajax/libs/prism/1.6.0';

	let doc = `<html>
	<head><title>view-source:${sourceTitle}</title>
		<link rel="stylesheet" href="${prismCDN}/themes/prism.min.css">
		<link rel="stylesheet" href="${prismCDN}/plugins/toolbar/prism-toolbar.min.css">
		<link rel="stylesheet" href="${prismCDN}/plugins/normalize-whitespace/prism-normalize-whitespace.min.js">
	</head>
	<body style="margin:0">
		<pre style="margin:0"><code class="language-markup">${source}</code></pre>
		<script src="${prismCDN}/prism.min.js"></script>
		<script src="${prismCDN}/components/prism-markup.min.js"></script>
	</body></html>`;
	let dataurl = `data:text/html;utf-8,${doc}`;

	window.open (dataurl);

})(document);