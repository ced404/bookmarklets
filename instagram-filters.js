// Instagram Filters Bookmarklet

(function(){
	"use strict";

	var Body = document.getElementsByTagName('body')[0],
			Images = document.getElementsByTagName('img'),
			l = Images.length;

	// Instagram CSS Filters - Matthew Blode - http://codepen.io/mblode/pen/XJggvW/
	var instagramFilters = [ 	{ "class" : "ig-xpro2", 		"css" : "-webkit-filter: contrast(1.3) brightness(0.8) sepia(0.3) saturate(1.5) hue-rotate(-20deg); filter: contrast(1.3) brightness(0.8) sepia(0.3) saturate(1.5) hue-rotate(-20deg);" },
														{ "class" : "ig-willow", 		"css" : "-webkit-filter: saturate(0.02) contrast(0.85) brightness(1.2) sepia(0.02); filter: saturate(0.02) contrast(0.85) brightness(1.2) sepia(0.02);" },
														{ "class" : "ig-walden",		"css" : "-webkit-filter: sepia(0.35) contrast(0.9) brightness(1.1) hue-rotate(-10deg) saturate(1.5); filter: sepia(0.35) contrast(0.9) brightness(1.1) hue-rotate(-10deg) saturate(1.5);" },
														{ "class" : "ig-valencia",	"css" : "-webkit-filter: sepia(0.15) saturate(1.5) contrast(0.9); filter: sepia(0.15) saturate(1.5) contrast(0.9);" },
														{ "class" : "ig-toaster", 	"css" : "-webkit-filter:sepia(0.4) saturate(2.5) hue-rotate(-30deg) contrast(0.67); -filter:sepia(0.4) saturate(2.5) hue-rotate(-30deg) contrast(0.67);"},
														{ "class" : "ig-sutro", 		"css" : "-webkit-filter: brightness(0.75) contrast(1.3) sepia(0.5) hue-rotate(-25deg); filter: brightness(0.75) contrast(1.3) sepia(0.5) hue-rotate(-25deg);" },
														{ "class" : "ig-sierra", 		"css" : "-webkit-filter: contrast(0.8) saturate(1.2) sepia(0.15); filter: contrast(0.8) saturate(1.2) sepia(0.15);" },
														{ "class" : "ig-rise", 			"css" : "-webkit-filter: saturate(1.4) sepia(0.25) hue-rotate(-15deg) contrast(0.8) brightness(1.1); filter: saturate(1.4) sepia(0.25) hue-rotate(-15deg) contrast(0.8) brightness(1.1);"},
														{ "class" : "ig-nashville", "css" : "-webkit-filter: sepia(0.4) saturate(1.5) contrast(0.9) brightness(1.1) hue-rotate(-15deg);filter: sepia(0.4) saturate(1.5) contrast(0.9) brightness(1.1) hue-rotate(-15deg);" },
														{ "class" : "ig-mayfair", 	"css" : "-webkit-filter: saturate(1.4) contrast(1.1); filter: saturate(1.4) contrast(1.1);" },
														{ "class" : "ig-lofi", 			"css" : "filter: contrast(1.4) brightness(0.9) sepia(0.05); -webkit-filter: contrast(1.4) brightness(0.9) sepia(0.05);" },
														{ "class" : "ig-kelvin", 		"css" : "filter: sepia(0.4) saturate(2.4) brightness(1.3) contrast(1); -webkit-filter: sepia(0.4) saturate(2.4) brightness(1.3) contrast(1);" },
														{ "class" : "ig-inkwell", 	"css" : "-webkit-filter: grayscale(1) brightness(1.2) contrast(1.05); filter: grayscale(1) brightness(1.2) contrast(1.05);" },
														{ "class" : "ig-hudson", 		"css" : "-webkit-filter: contrast(1.2) brightness(0.9) hue-rotate(-10deg); filter: contrast(1.2) brightness(0.9) hue-rotate(-10deg);" },
														{ "class" : "ig-hefe",			"css" : "-webkit-filter: contrast(1.3) sepia(0.3) saturate(1.3) hue-rotate(-10deg) brightness(0.95); filter: contrast(1.3) sepia(0.3) saturate(1.3) hue-rotate(-10deg) brightness(0.95);"},
														{ "class" : "ig-earlybird",	"css" : "-webkit-filter: sepia(0.4) saturate(1.6) contrast(1.1) brightness(0.9) hue-rotate(-10deg); filter: sepia(0.4) saturate(1.6) contrast(1.1) brightness(0.9) hue-rotate(-10deg);" },
														{ "class" : "ig-brannan", 	"css" : "-webkit-filter: sepia(0.5) contrast(1.4); filter: sepia(0.5) contrast(1.4);" },
														{ "class" : "ig-amaro",			"css" : "-webkit-filter: hue-rotate(-10deg) contrast(0.9) brightness(1.1) saturate(1.5); filter: hue-rotate(-10deg) contrast(0.9) brightness(1.1) saturate(1.5);" },
														{ "class" : "ig-1977", 			"css" : "-webkit-filter: sepia(0.5) hue-rotate(-30deg) saturate(1.2) contrast(0.8); filter: sepia(0.5) hue-rotate(-30deg) saturate(1.2) contrast(0.8);" },

													];

	var FilterList = document.createElement('ul');
			FilterList.classList.add('list', 'list--filters');


	var init = function() {

		// ui styles
		var styles = '.list--filters.is-visible { display:block } \
		 							.list--filters { \
											background: white;\
											box-sizing: border-box; \
											box-shadow: 0 1px 6px -2px rgba(0,0,0,.5);\
											display: none;\
											font-weight: 200;\
											font-size : 1rem;\
											line-height : 1.5;\
											font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif;\
											left: 0;\
											list-style: none;\
											margin: 0;\
											max-height: 18rem;\
											opacity:.92;\
											padding: 1rem;\
											position: absolute;\
											top: 0;\
											width: 18rem;\
											overflow-y: auto; \
											z-index: 2147483647;}\
									.item--filter {\
											line-height: 2em;\
											cursor: default;\
											color:black;\
											background: inherit;\
											padding: 0 1em;\
									}\
									.item.item--filter:hover {\
											-webkit-filter: invert(100%);\
									}\
									.item--filter.is-active {\
										background: #FF4500;\
										color:white;\
									}\
									.close-filters {\
											pointer-events: none;\
											float: right;\
											font-size: 1.5em;\
}';


		// build menu and filters stylesheet

		var Item = document.createElement('li');
				Item.dataset.filter = 'close-filter';
				Item.classList.add('item', 'item--filter');
				Item.innerHTML = '<strong>Instagram Filters!</strong>&nbsp;<span class="close-filters">×</span>';

		FilterList.appendChild(Item);

		var Item = document.createElement('li');
				Item.dataset.filter = 'no-filter';
				Item.classList.add('item', 'item--filter');
				Item.innerText = 'No filter';

		FilterList.appendChild(Item);


		var lf = instagramFilters.length;
		while(lf--) {
			styles += "." + instagramFilters[lf].class + "{" + instagramFilters[lf].css + "}";

			var Item = document.createElement('li');
					Item.dataset.filter = instagramFilters[lf].class;
					Item.classList.add('item', 'item--filter');
					Item.innerText = instagramFilters[lf].class;

			FilterList.appendChild(Item);
		}

		// inject all
		addStyle( styles );
		Body.appendChild( FilterList );

		// bind events on filter list
		FilterList.addEventListener('click', applyFilter );

		// bind events on images
		while (l--) {
			Images[l].dataset.imageId = 'image_' + l;
			Images[l].addEventListener('contextmenu', function (e){ return showMenu(e) });
		}
	};


	var showMenu = function (e) {
		e.preventDefault();
		console.log('showMenu');

		var	scrollX = window.pageXOffset,
      scrollY = window.pageYOffset,
		 		left = parseInt( e.clientX + scrollX ) + "px",
		 		top = parseInt( e.clientY + scrollY ) + "px";

		FilterList.style.left = left;
		FilterList.style.top = top;

		var Image = e.target;
		FilterList.dataset.imageId = Image.dataset.imageId;
		FilterList.classList.add('is-visible');
	};


	var hideMenu = function (e) {
		FilterList.classList.remove('is-visible');
	};


	var applyFilter = function (e) {
		e.preventDefault();

		console.log('applyFilter');

		if( e.target && e.target.dataset.filter ) {

			var filterClass = e.target.dataset.filter,
					imageId = e.target.parentElement.dataset.imageId;

			if ( filterClass == 'close-filter' )
				return hideMenu();

			var Items = document.querySelectorAll('.item--filter'),
					li = Items.length;

			while (li--)
				Items[li].classList.remove('is-active');

			e.target.classList.add('is-active');

			var Image = document.querySelector( 'img[data-image-id="'+imageId+'"]' ),
					currentFilter = Image.dataset.currentFilter;

			if( currentFilter )
				Image.classList.remove( currentFilter );

			if ( filterClass == 'no-filter' )
				return;

			Image.dataset.currentFilter = filterClass;
			Image.classList.add( filterClass );

			console.log (Image, filterClass, imageId);
		}
	};


	var addStyle = function(str) {
		var node = document.createElement('style');
		node.innerHTML = str;
		node.setAttribute('id','editmode');
		document.body.appendChild(node);
	};


	init();


})();
