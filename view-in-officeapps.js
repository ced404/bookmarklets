
/*
	Open links in Office Apps

	cmd + click / ctrl + click / shift + click any link to open URL in Office Online

	javascript:for(var links=document.querySelectorAll("a"),l=links.length,viewInOfficeOnline=function(e){return e.metaKey||e.ctrlKey||e.shiftKey?(e.preventDefault(),window.open(e.target.dataset.officeUrl)):void 0};l--;){var officeURL="https://view.officeapps.live.com/op/view.aspx?src="+links[l].getAttribute("href");links[l].dataset.officeUrl=officeURL,links[l].addEventListener("click",viewInOfficeOnline)}

*/

var links = document.querySelectorAll ('a'),
		l = links.length;

var viewInOfficeOnline = function (event) {
	if ( event.metaKey || event.ctrlKey || event.shiftKey ) {
		event.preventDefault();
		return window.open ( event.target.dataset.officeUrl );
	}
};

while (l--) {
	var officeURL = 'https://view.officeapps.live.com/op/view.aspx?src=' + links[l].getAttribute('href');
	links[l].dataset.officeUrl = officeURL;
	links[l].addEventListener ('click', viewInOfficeOnline);
}
