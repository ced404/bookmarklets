

var jsPad = {
	"style" : ".console{--maxHeight:30vh;position:fixed;bottom:0;background-color:#1f1f21;width:100vw;padding:1em;color:#c9c9d6;border-top:1px solid gray;overflow-y:auto;height:10ch;will-change:max-height height;max-height:0;animation:showConsole ease-out .5s forwards;transition:height ease-out .3s;white-space:pre-line;z-index:666}.console:hover{height:var(--maxHeight)}@keyframes showConsole{from{max-height:0vh}to{max-height:var(--maxHeight)}}",
};

jsPad.addStyle = function (str) {
	var node = document.createElement ('style');
	node.innerHTML = str;
	document.body.appendChild (node);
};

jsPad.init = function () {
	var node = document.createElement ('div');
	node.classList.add ('console', 'scrollbar');
	node.dataset.js = 'console-output';
	document.body.appendChild (node);
	this.consoleElement = node;
	this.addStyle (this.style);
	this.captureConsole ();
};

jsPad.ouputConsole = function (message, arguments) {
	var node = document.createElement ('p');
	node.innerHTML = message;
	node.classList.add ('entry');
	this.consoleElement.appendChild (node);
	// jsPad.consoleElement.innerText += message + "\n";
};

jsPad.captureConsole = function () {
	var oldLog = console.log;

	var logger = function (message) {
		jsPad.ouputConsole (message, arguments);
		oldLog.apply (console, arguments);
	};

	console.log = logger;
	console.info = logger;
	console.warn = logger;
	console.error = logger;

	// console.log = function (message) {
	// 	jsPad.ouputConsole (message);
	// 	oldLog.apply(console, arguments);
	// };
};

jsPad.init ();
