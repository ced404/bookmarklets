var convertSelection = function (textCase) {

	textCase = textCase || 'lower';

	var sel = window.getSelection(),
			el = document.activeElement, // sel.getRangeAt(0).startContainer.parentNode, // sel.focusNode[0],
			selection = window.getSelection().toString();

	console.table (selection);


	if ( undefined !== selection && el.nodeName.toLowerCase() == "textarea" || el.nodeName.toLowerCase() == "input" ) {

		el.value = changeCase (textCase, selection);

	}
	else if ( undefined !== selection ) {

		var text = changeCase (textCase, selection);

		if (sel.rangeCount) {
			range = sel.getRangeAt (0);
			range.deleteContents ();
			range.insertNode ( document.createTextNode(text) );
		}

	}

}



var changeCase = function (textCase, text) {

	switch (textCase) {

		case 'lower':
			return text.replace ( text, text.toLowerCase() );
		break;

		case 'upper':
			return text.replace ( text, text.toUpperCase() );
		break;

		case 'title':
			return text.replace ( text, toTitleCase (text) );
		break;

	}
	return '';
}



var toTitleCase = function (str, glue) {
	str = str.toLowerCase();
	glue = (glue) ? glue : ['of', 'for', 'and', 'le', 'la', 'les', 'du', 'de', 'des', 'et', 'rue'];
	return str.replace(/(\w)(\w*)/g, function(_, i, r){
		var j = i.toUpperCase() + (r != null ? r : "");
		return (glue.indexOf(j.toLowerCase())<0)?j:j.toLowerCase();
	});
};


convertSelection ('title');



/*
Bookmarklets

	lowercase:

	javascript:var convertSelection=function(e){e=e||"lower";var t=window.getSelection(),n=document.activeElement,o=window.getSelection().toString();if(console.table(o),void 0!==o&&"textarea"==n.nodeName.toLowerCase()||"input"==n.nodeName.toLowerCase())n.value=changeCase(e,o);else if(void 0!==o){var r=changeCase(e,o);t.rangeCount&&(range=t.getRangeAt(0),range.deleteContents(),range.insertNode(document.createTextNode(r)))}},changeCase=function(e,t){switch(e){case"lower":return t.replace(t,t.toLowerCase());case"upper":return t.replace(t,t.toUpperCase());case"title":return t.replace(t,toTitleCase(string))}return""},toTitleCase=function(e,t){return e=e.toLowerCase(),t=t?t:["of","for","and","le","la","les","du","de","des","et","rue"],e.replace(/(\w)(\w*)/g,function(e,n,o){var r=n.toUpperCase()+(null!=o?o:"");return t.indexOf(r.toLowerCase())<0?r:r.toLowerCase()})};convertSelection("lower");

	uppercase:

	javascript:var convertSelection=function(e){e=e||"lower";var t=window.getSelection(),n=document.activeElement,o=window.getSelection().toString();if(console.table(o),void 0!==o&&"textarea"==n.nodeName.toLowerCase()||"input"==n.nodeName.toLowerCase())n.value=changeCase(e,o);else if(void 0!==o){var r=changeCase(e,o);t.rangeCount&&(range=t.getRangeAt(0),range.deleteContents(),range.insertNode(document.createTextNode(r)))}},changeCase=function(e,t){switch(e){case"lower":return t.replace(t,t.toLowerCase());case"upper":return t.replace(t,t.toUpperCase());case"title":return t.replace(t,toTitleCase(string))}return""},toTitleCase=function(e,t){return e=e.toLowerCase(),t=t?t:["of","for","and","le","la","les","du","de","des","et","rue"],e.replace(/(\w)(\w*)/g,function(e,n,o){var r=n.toUpperCase()+(null!=o?o:"");return t.indexOf(r.toLowerCase())<0?r:r.toLowerCase()})};convertSelection("upper");

	title case:

	javascript:var convertSelection=function(e){e=e||"lower";var t=window.getSelection(),n=document.activeElement,o=window.getSelection().toString();if(console.table(o),void 0!==o&&"textarea"==n.nodeName.toLowerCase()||"input"==n.nodeName.toLowerCase())n.value=changeCase(e,o);else if(void 0!==o){var r=changeCase(e,o);t.rangeCount&&(range=t.getRangeAt(0),range.deleteContents(),range.insertNode(document.createTextNode(r)))}},changeCase=function(e,t){switch(e){case"lower":return t.replace(t,t.toLowerCase());case"upper":return t.replace(t,t.toUpperCase());case"title":return t.replace(t,toTitleCase(t))}return""},toTitleCase=function(e,t){return e=e.toLowerCase(),t=t?t:["of","for","and","le","la","les","du","de","des","et","rue"],e.replace(/(\w)(\w*)/g,function(e,n,o){var r=n.toUpperCase()+(null!=o?o:"");return t.indexOf(r.toLowerCase())<0?r:r.toLowerCase()})};convertSelection("title");

*/
