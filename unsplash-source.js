/*

	convert the current picture url to source.unsplash.com

	https://unsplash.com/photos/gUBJ9vSlky0 -> https://source.unsplash.com/gUBJ9vSlky0/{size}

	https://regex101.com/r/tSYhV1/1

	javascript:!function(){var e={size:"1000x400",pictureID:"xekxE_VR0Ec",sourceURL:"source.unsplash.com/{pictureID}/{size}"},c=window.location.href,o=/unsplash\.com\/.*?photos*(=|\/)([a-zA-Z0-9]+)/gi,r=function(c,o,r){var i=e.sourceURL.replace("{pictureID}",r);return i=i.replace("{size}",e.size)};window.location.href=c.replace(o,r)}();
*/

(function () {

	var defaults = {
		size: '1000x400',
		pictureID: 'xekxE_VR0Ec',
		sourceURL: 'source.unsplash.com/{pictureID}/{size}',
	},

	currentURL = window.location.href, //'https://unsplash.com/search/robot?photo=qLiFcanSpuA',
	regex = /unsplash\.com\/.*?photos*(=|\/)([a-zA-Z0-9]+)/gi,

	matcher = function (match, $1, $2) {
		var sourceURL = defaults.sourceURL.replace ('{pictureID}', $2);
		sourceURL = sourceURL.replace ('{size}', defaults.size);
		return sourceURL;
	};

	window.location.href = currentURL.replace (regex, matcher);

})();
