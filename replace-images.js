// http://placehold.it/350x150/ff00ff/ffffff
var imgs = document.getElementsByTagName('img');
var l = imgs.length;

while (l--) {
	var img = imgs[l];
	var imgWidth = window.getComputedStyle(img).width;
	var imgHeight = window.getComputedStyle(img).height;

	var placeholder = 'http://placehold.it/350x150/ff00ff/ffffff';
	placeholder = placeholder.replace ('350', imgWidth );
	placeholder = placeholder.replace ('150', imgHeight );
	placeholder = placeholder.replace('px','');

	console.info ( img.getAttribute ('alt'), img.getAttribute ('src'));

	img.setAttribute('src', placeholder);
	img.removeAttribute('srcset');
}


// javascript:for(var imgs=document.getElementsByTagName("img"),l=imgs.length;l--;){var img=imgs[l],imgWidth=window.getComputedStyle(img).width,imgHeight=window.getComputedStyle(img).height,placeholder="http://placehold.it/350x150/ff00ff/ffffff";placeholder=placeholder.replace("350",imgWidth),placeholder=placeholder.replace("150",imgHeight),placeholder=placeholder.replace("px",""),console.info(img.getAttribute("alt"),img.getAttribute("src")),img.setAttribute("src",placeholder),img.removeAttribute("srcset")}
