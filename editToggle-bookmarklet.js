var de = document.body.contentEditable,
		dd = document.designMode,
		addStyle = function(str) {
			var node = document.createElement('style');
			node.innerHTML = str;
			node.setAttribute('id','editmode');
			document.body.appendChild(node);
		};

if( document.querySelector('#editmode') ) {
	document.querySelector('#editmode').remove();
}
else {
	addStyle(".js-editable:before{ content: 'edit mode on'; position: absolute; top: 0; left: 0; z-index: 666999; background: orange; color: white; padding: 2px 6px; font: 200 10px arial,sans-serif;}");
}

if( de != 'inherit' && de != 'false' ) {
	document.body.removeAttribute('spellcheck');
	document.body.contentEditable = 'false';
	document.designMode = 'off';
	document.body.classList.remove('js-editable');
}
else {
	document.body.setAttribute('spellcheck','false');
	document.body.contentEditable = 'true';
	document.designMode = 'on';
	document.body.classList.add('js-editable');
}
